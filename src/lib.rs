use std::fmt::Display;
use std::sync::mpsc;
use std::sync::Arc;
use std::sync::Mutex;
use std::thread;

// TODO: stopped at: #creating-space-to-store-the-threads

#[derive(Debug, Clone)]
pub struct PoolCreationError {
    reason: PoolCreationErrorReason,
}

#[derive(Debug, Clone)]
enum PoolCreationErrorReason {
    InvalidSize(usize),
}

impl PoolCreationError {
    fn invalid_size(count: usize) -> PoolCreationError {
        PoolCreationError {
            reason: PoolCreationErrorReason::InvalidSize(count),
        }
    }
}

impl Display for PoolCreationError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "Could not create Pool. Reason: {}",
            match self.reason {
                PoolCreationErrorReason::InvalidSize(count) =>
                    format!("{} is not a valid thread count!", count),
            }
        )
    }
}

type Job = Box<dyn FnOnce() + Send + 'static>;

enum Message {
    NewJob(Job),
    Terminate,
}

pub struct ThreadPool {
    threads: Vec<Worker>,
    sender: mpsc::Sender<Message>,
}

impl ThreadPool {
    /// Create a new ThreadPool
    ///
    /// The size is the number of threads in the pool
    ///
    /// # Examples
    ///
    /// ```
    /// // This test will return a PoolCreationError
    /// use webserver_rs::ThreadPool;
    /// let pool = ThreadPool::new(0);
    /// assert!(pool.is_err());
    /// ```
    ///
    /// # Panics
    ///
    /// The `new` function will panic, if the size is 0.
    pub fn new(size: usize) -> Result<ThreadPool, PoolCreationError> {
        if size <= 0 {
            Err(PoolCreationError::invalid_size(size))
        } else {

            let (sender, receiver) = mpsc::channel();
            let receiver = Arc::new(Mutex::new(receiver));

            let mut threads = Vec::with_capacity(size);

            for id in 0..size {
                threads.push(Worker::new(id, Arc::clone(&receiver)));
            }

            Ok(ThreadPool { threads, sender })
        }
    }

    pub fn execute<F>(&self, f: F)
    where
        F: FnOnce() + Send + 'static,
    {
        let job = Box::new(f);

        self.sender.send(Message::NewJob(job)).unwrap();
    }
}

impl Drop for ThreadPool {
    fn drop(&mut self) {
        println!("Notifying all workers of shutdown.");

        for _ in &self.threads {
            self.sender.send(Message::Terminate).unwrap();
        }

        println!("Waiting for all workers to shut down");

        for w in &mut self.threads {
            println!("Waiting for worker {} to shut down", w.id);

            if let Some(thread) = w.thread.take() {
                thread.join().unwrap();
            }
        }
    }
}

struct Worker {
    id: usize,
    thread: Option<thread::JoinHandle<()>>,
}

impl Worker {
    fn new(id: usize, receiver: Arc<Mutex<mpsc::Receiver<Message>>>) -> Worker {
        let thread = thread::spawn(move || loop {
            let msg = receiver.lock().unwrap().recv().unwrap();
            match msg {
                Message::NewJob(job) => {
                    println!("Worker {} got a job; executing.", id);
                    let addr = &job as *const Job;
                    println!("Job has address {:?}", addr);

                    job();
                },
                Message::Terminate => {
                    println!("Worker {} was told to terminate.", id);

                    break;
                }
            }
        });

        Worker {
            id,
            thread: Some(thread)
        }

        // : Some(thread::spawn(move || loop {
        //     let job = receiver.lock().unwrap().recv().unwrap();

        //     match job {
        //         Message::NewJob(job) => {
        //             println!("Worker {} got a job. Executing...", id);
        //             job()
        //         }
        //         Message::Terminate => {
        //             println!("Worker {} was told to terminate.", id);
        //             break;
        //         }
        //     }
        // })),
    }
}
