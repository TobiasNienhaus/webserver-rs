use std::fs;
use std::io::prelude::*;
use std::net::{TcpListener, TcpStream};
use std::time::Duration;
use webserver_rs::ThreadPool;

fn main() {
    let listener = TcpListener::bind("127.0.0.1:7878").unwrap();
    let pool = ThreadPool::new(4).unwrap();

    for stream in listener.incoming().take(2) {
        let stream = stream.unwrap();

        // std::thread::spawn(|| handle_stream(stream));
        pool.execute(|| handle_stream(stream));
    }

    println!("Shutting down");
}

fn handle_stream(mut stream: TcpStream) {
    let mut buffer = [0; 4096];

    stream.read(&mut buffer).unwrap();

    let req = String::from_utf8_lossy(&buffer);
    println!("REQUEST:\n{}", req);

    let response = {
        let get = b"GET / HTTP/1.1\r\n";
        let sleep = b"GET /sleep HTTP/1.1\r\n";
        let (status, file) = if buffer.starts_with(get) {
            println!("normal");
            ("HTTP/1.1 200 OK\r\n", "static/index.html")
        } else if buffer.starts_with(sleep) {
            println!("Sleep");
            std::thread::sleep(Duration::from_secs(10));
            ("HTTP/1.1 200 OK\r\n", "static/sleep.html")
        } else {
            println!("404");
            ("HTTP/1.1 404 Not Found\r\n", "static/404.html")
        };
        println!("STATUS: {}", status);
        let file = fs::read_to_string(file).unwrap();
        format!("{}Content-Length: {}\r\n\r\n{}", status, file.len(), file)
    };

    stream.write(response.as_bytes()).unwrap();
    stream.flush().unwrap();
}
